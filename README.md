# Bake

**BAKE IS IN PRE-ALPHA**

Bake is a code generation tool, inspired by [Wire](http://github.com/google/wire):

  1. Bake is easy to write, execute, and understand.
  2. Bake recipes are written in idiomatic Go.
  3. Bake does not need special comments, syntax, configuration, etc.

## Limitations

  * Types used as type parameters must be in the same project as generic placeholders
  * Generic recipies/generators must be in the same file as generic placeholders

## Generics

Bake can be used to implement compile-time parametric polymorphism, also known as type
parameterization or generics.

```go
// +build bakegenerate

package pkg

import "gitlab.com/firelizzard/bake/generic"

func recipe(generic generic.GeneratorContext) {
    // create a version of GenericFunction for each of CustomInt, CustomFloat, and CustomDecimal
    generic.Function(GenericType).
        Parameterize(Numeric(0)).
        Generate("CustomInt", (*big.Int)(0)).
        Generate("CustomFloat", (*big.Float)(0)).
        Generate("CustomDecimal", (*big.Rat)(0))

    // create a version of GenericFunction for each of CustomInt, CustomFloat, and CustomDecimal
    generic.Function(GenericFunction).
        Parameterize(Numeric(0)).
        Generate("GenericIntFunction", CustomInt(0)).
        Generate("GenericFloatFunction", CustomFloat(0)).
        Generate("GenericDecimalFunction", CustomDecimal(0))

    // create a version of Numeric.GenericMethod for each of CustomInt, CustomFloat, and CustomDecimal
    generic.Method(Numeric(0).GenericMethod).
        Generate(CustomInt(0)).
        Generate(CustomFloat(0)).
        Generate(CustomDecimal(0))
}

type Numeric int

type GenericType Numeric

// placeholders, since Custom* are not generated yet
type (
    CustomInt interface{}
    CustomFloat interface{}
    CustomDecimal interface{}
)

func GenericFunction(x, y Numeric) Numeric {
    if x < y {
        return x
    }
    return y
}

func (x Numeric) GenericMethod(y Numeric) int {
    if x < y {
        return -1
    }
    if x > y {
        return +1
    }
    return 0
}
```