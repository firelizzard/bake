module gitlab.com/firelizzard/bake

go 1.12

require (
	github.com/fatih/astrewrite v0.0.0-20180730114054-bef4001d457f
	golang.org/x/tools v0.0.0-20190308174544-00c44ba9c14f
)
