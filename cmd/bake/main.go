package main

import (
	"context"
	"fmt"
	"log"
	"os"
	"strings"

	"gitlab.com/firelizzard/bake/internal/bake"
)

func main() {
	if len(os.Args) > 1 && os.Args[1] == "help" {
		fmt.Println("No help yet")
		return
	}

	wd, err := os.Getwd()
	if err != nil {
		log.Fatal(err)
	}

	if len(os.Args) > 1 {
		wd = os.Args[1]
	}

	errs := bake.Generate(context.Background(), wd, os.Environ())
	if len(errs) > 0 {
		logErrors(errs)
		return
	}
}

func logErrors(errs []error) {
	for _, err := range errs {
		fmt.Fprintln(os.Stderr, strings.Replace(err.Error(), "\n", "\n\t", -1))
	}
}
