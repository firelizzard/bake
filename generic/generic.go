package generic

// Generator is a function that specifies how generic method definitions should
// be transformed into concrete implementations.
type Generator func(GeneratorContext)

// GeneratorContext is a placeholder that is used to write generators.
type GeneratorContext interface {
	Type(interface{}) GenericTypeDefinition
	Method(interface{}) GenericMethodDefinition
	Function(interface{}) GenericFunctionDefinition
}

// GenericTypeDefinition is used to instantiate concrete implementations of
// generic types.
type GenericTypeDefinition interface {
	Parameterize(types ...interface{}) GenericTypeDefinition

	Generate(name string, types ...interface{}) GenericTypeDefinition
}

// GenericMethodDefinition is used to instantiate concrete implementations of
// generic methods.
type GenericMethodDefinition interface {
	Parameterize(types ...interface{}) GenericMethodDefinition

	Generate(receiverType interface{}, types ...interface{}) GenericMethodDefinition
}

// GenericFunctionDefinition is used to instantiate concrete implementations of
// generic functions.
type GenericFunctionDefinition interface {
	Parameterize(types ...interface{}) GenericFunctionDefinition

	Generate(name string, types ...interface{}) GenericFunctionDefinition
}
