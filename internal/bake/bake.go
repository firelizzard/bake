package bake

import (
	"context"
	"fmt"
	"go/ast"
	"io"
	"io/ioutil"
	"os"
	"path"
	"path/filepath"
	"strconv"

	"golang.org/x/tools/go/packages"
)

// A Generator generates code.
type Generator interface {
	Generate(*packages.Package, definitionLookup, *ast.File, io.Writer) []error
}

// Generate loads the specified packages, locates generators, and executes them.
func Generate(ctx context.Context, wd string, env []string) []error {
	pkgs, errs := load(context.Background(), wd, os.Environ())
	if len(errs) > 0 {
		return errs
	}

	var allErrs = []error{}
	var allGenerators = map[*packages.Package]map[*ast.File][]Generator{}
	var defs = definitionLookup{}

	for _, pkg := range pkgs {
		allGenerators[pkg], errs = parse(pkg, defs)
		if len(errs) != 0 {
			allErrs = append(allErrs, errs...)
		}
	}

	if len(allErrs) > 0 {
		return allErrs
	}

generate:
	for pkg, pkgGenerators := range allGenerators {
		if len(pkg.GoFiles) == 0 {
			allErrs = append(allErrs, fmt.Errorf("cannot determine output directory: package '%s' has no files", pkg.Name))
			continue
		}

		var dir = filepath.Dir(pkg.GoFiles[0])
		for _, f := range pkg.GoFiles[1:] {
			dir2 := filepath.Dir(f)
			if dir2 != dir {
				allErrs = append(allErrs, fmt.Errorf("cannot determine output directory: package '%s' has files in multiple directories", pkg.Name))
				continue generate
			}
		}

		w := ioutil.Discard
		file, err := os.Create(path.Join(dir, "bake_gen.go"))
		if err != nil {
			allErrs = append(allErrs, err)
		} else {
			w = file
		}

		imports := map[string]struct{}{}
		for src := range pkgGenerators {
			for _, i := range src.Imports {
				pkg, err := strconv.Unquote(i.Path.Value)
				if err != nil {
					continue
				}
				if _, ok := resolveBakeImport(pkg); ok {
					continue
				}
				s := i.Path.Value
				if i.Name != nil {
					s = i.Name.String() + " " + s
				}
				imports[s] = struct{}{}
			}
		}

		fmt.Fprintf(w, "// +build !bakegenerate\n\n")
		fmt.Fprintf(w, "package %v\n\n", pkg.Name)

		fmt.Fprintf(w, "//go:generate bake\n\n")

		if len(imports) > 0 {
			fmt.Fprintf(w, "import (\n")
			for i := range imports {
				fmt.Fprintf(w, "\t%s\n", i)
			}
			fmt.Fprintf(w, ")\n\n")
		}

		for src, srcGenerators := range pkgGenerators {
			for _, g := range srcGenerators {
				errs := g.Generate(pkg, defs, src, w)
				if len(errs) != 0 {
					allErrs = append(allErrs, errs...)
				}
			}
		}

		if file == w {
			err = file.Close()
			if err != nil {
				allErrs = append(allErrs, err)
			}
		}
	}

	return allErrs
}

func load(ctx context.Context, wd string, env []string) ([]*packages.Package, []error) {
	cfg := &packages.Config{
		Context:    ctx,
		Mode:       packages.LoadSyntax,
		Dir:        wd,
		BuildFlags: []string{"-tags=bakegenerate"},
	}

	pkgs, err := packages.Load(cfg)
	if err != nil {
		return nil, []error{err}
	}
	var errs []error
	for _, p := range pkgs {
		for _, e := range p.Errors {
			errs = append(errs, e)
		}
	}
	if len(errs) > 0 {
		return nil, errs
	}
	return pkgs, nil
}
