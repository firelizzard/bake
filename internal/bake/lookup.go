package bake

import (
	"go/ast"
	"go/types"
)

type definitions struct {
	Specs []ast.Spec
	Decls []ast.Decl
}

func (d *definitions) AddSpec(spec ast.Spec) {
	if d.Specs == nil {
		d.Specs = []ast.Spec{spec}
	} else {
		d.Specs = append(d.Specs, spec)
	}
}

func (d *definitions) AddDecl(decl ast.Decl) {
	if d.Decls == nil {
		d.Decls = []ast.Decl{decl}
	} else {
		d.Decls = append(d.Decls, decl)
	}
}

type definitionLookup map[types.Object]*definitions

func (l definitionLookup) ensure(id types.Object) *definitions {
	def, ok := l[id]
	if ok {
		return def
	}

	def = new(definitions)
	l[id] = def
	return def
}

func (l definitionLookup) AddSpec(info *types.Info, spec ast.Spec) bool {
	return processSpec(info, spec, func(id types.Object) {
		l.ensure(id).AddSpec(spec)
	})
}

func (l definitionLookup) AddDecl(info *types.Info, decl ast.Decl) bool {
	return processDecl(info, decl, func(id types.Object) {
		l.ensure(id).AddDecl(decl)
	}, func(id types.Object, spec ast.Spec) {
		l.ensure(id).AddSpec(spec)
	})
}

func processSpec(info *types.Info, spec ast.Spec, cb func(types.Object)) bool {
	switch spec := spec.(type) {
	case *ast.ImportSpec:
		id := info.ObjectOf(spec.Name)
		if id == nil {
			return false
		}
		cb(id)
		return true

	case *ast.TypeSpec:
		id := info.ObjectOf(spec.Name)
		if id == nil {
			return false
		}
		cb(id)
		return true

	case *ast.ValueSpec:
		r := false
		for _, name := range spec.Names {
			id := info.ObjectOf(name)
			if id == nil {
				continue
			}
			r = true
			cb(id)
		}
		return r
	}

	return false
}

func processDecl(info *types.Info, decl ast.Decl, cbDecl func(types.Object), cbSpec func(types.Object, ast.Spec)) bool {
	switch decl := decl.(type) {
	case *ast.FuncDecl:
		id := info.ObjectOf(decl.Name)
		if id == nil {
			return false
		}
		cbDecl(id)
		return true

	case *ast.GenDecl:
		r := false
		for _, spec := range decl.Specs {
			s := processSpec(info, spec, func(id types.Object) {
				cbDecl(id)
				cbSpec(id, spec)
			})
			r = r || s
		}
		return r
	}

	return false
}

func (l definitionLookup) FindFunction(id types.Object) *ast.FuncDecl {
	d, ok := l[id]
	if !ok || len(d.Decls) < 1 {
		return nil
	}

	for _, d := range d.Decls {
		if e, ok := d.(*ast.FuncDecl); ok {
			return e
		}
	}
	return nil
}

func (l definitionLookup) FindGenericDeclaration(id types.Object) *ast.GenDecl {
	d, ok := l[id]
	if !ok || len(d.Decls) < 1 {
		return nil
	}

	for _, d := range d.Decls {
		if e, ok := d.(*ast.GenDecl); ok {
			return e
		}
	}
	return nil
}

func (l definitionLookup) FindImport(id types.Object) *ast.ImportSpec {
	d, ok := l[id]
	if !ok || len(d.Specs) < 1 {
		return nil
	}

	for _, s := range d.Specs {
		if r, ok := s.(*ast.ImportSpec); ok {
			return r
		}
	}
	return nil
}

func (l definitionLookup) FindType(id types.Object) *ast.TypeSpec {
	d, ok := l[id]
	if !ok || len(d.Specs) < 1 {
		return nil
	}

	for _, s := range d.Specs {
		if r, ok := s.(*ast.TypeSpec); ok {
			return r
		}
	}
	return nil
}

func (l definitionLookup) FindValue(id types.Object) *ast.ValueSpec {
	d, ok := l[id]
	if !ok || len(d.Specs) < 1 {
		return nil
	}

	for _, s := range d.Specs {
		if r, ok := s.(*ast.ValueSpec); ok {
			return r
		}
	}
	return nil
}
