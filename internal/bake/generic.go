package bake

import (
	"fmt"
	"go/ast"
	"go/printer"
	"go/token"
	"go/types"
	"io"

	"github.com/fatih/astrewrite"
	"golang.org/x/tools/go/packages"
)

// verify that GenericType, GenericMethod and GenericFunction are Generators
var (
	_ Generator = new(GenericType)
	_ Generator = new(GenericMethod)
	_ Generator = new(GenericFunction)
)

type GenericType struct {
	Type       *types.TypeName
	TypeParams []*types.TypeName
	Targets    []*GenericTypeTarget
}

type GenericTypeTarget struct {
	Name     string
	TypeArgs []*types.TypeName
}

func (g *GenericType) Generate(pkg *packages.Package, defs definitionLookup, src *ast.File, w io.Writer) []error {
	spec := defs.FindType(g.Type)
	if spec == nil {
		return []error{fmt.Errorf("could not locate type declaration for '%v'", g.Type)}
	}

	decl := &ast.GenDecl{
		Tok:   token.TYPE,
		Specs: []ast.Spec{spec},
	}

	var typeParams = make([][]*ast.Ident, len(g.TypeParams))
	for i := range typeParams {
		typeParams[i] = []*ast.Ident{}
	}

	astrewrite.Walk(spec.Type, func(node ast.Node) (result ast.Node, cont bool) {
		result, cont = node, true

		id, ok := node.(*ast.Ident)
		if !ok {
			return
		}

		obj, ok := pkg.TypesInfo.ObjectOf(id).(*types.TypeName)
		if !ok {
			return
		}

		for i, typ := range g.TypeParams {
			if obj == typ {
				typeParams[i] = append(typeParams[i], id)
				return
			}
		}

		return
	})

	var errors = []error{}
	for i, typ := range typeParams {
		if typ == nil {
			errors = append(errors, fmt.Errorf("failed to locate identifier for type parameter %d", i))
		}
	}
	if len(errors) > 0 {
		return errors
	}

	for _, t := range g.Targets {
		spec.Name.Name = t.Name

		for i, typ := range typeParams {
			updateIdentNames(typ, getQualifiedObjectName(pkg.Types, t.TypeArgs[i]))
		}

		err := printer.Fprint(w, pkg.Fset, decl)
		if err != nil {
			errors = append(errors, err)
		} else {
			fmt.Fprintf(w, "\n\n")
		}
	}

	return errors
}

type GenericMethod struct {
	Method       *types.Func
	ReceiverType *types.TypeName
	TypeParams   []*types.TypeName
	Targets      []*GenericMethodTarget
}

type GenericMethodTarget struct {
	ReceiverType *types.TypeName
	TypeArgs     []*types.TypeName
}

func (g *GenericMethod) Generate(pkg *packages.Package, defs definitionLookup, src *ast.File, w io.Writer) []error {
	decl := defs.FindFunction(g.Method)
	if decl == nil {
		return []error{fmt.Errorf("could not locate method declaration for '%v'", g.Method)}
	}

	var receiver = []*ast.Ident{}
	var typeParams = make([][]*ast.Ident, len(g.TypeParams))
	for i := range typeParams {
		typeParams[i] = []*ast.Ident{}
	}

	astrewrite.Walk(decl, func(node ast.Node) (result ast.Node, cont bool) {
		result, cont = node, true

		id, ok := node.(*ast.Ident)
		if !ok {
			return
		}

		obj, ok := pkg.TypesInfo.ObjectOf(id).(*types.TypeName)
		if !ok {
			return
		}

		if obj == g.ReceiverType {
			receiver = append(receiver, id)
			return
		}

		for i, typ := range g.TypeParams {
			if obj == typ {
				typeParams[i] = append(typeParams[i], id)
				return
			}
		}

		return
	})

	var errors = []error{}
	if receiver == nil {
		errors = append(errors, fmt.Errorf("failed to locate identifier for receiver type"))
	}
	for i, typ := range typeParams {
		if typ == nil {
			errors = append(errors, fmt.Errorf("failed to locate identifier for type parameter %d", i))
		}
	}
	if len(errors) > 0 {
		return errors
	}

	for _, t := range g.Targets {
		updateIdentNames(receiver, getQualifiedObjectName(pkg.Types, t.ReceiverType))
		for i, typ := range typeParams {
			updateIdentNames(typ, getQualifiedObjectName(pkg.Types, t.TypeArgs[i]))
		}

		err := printer.Fprint(w, pkg.Fset, decl)
		if err != nil {
			errors = append(errors, err)
		} else {
			fmt.Fprintf(w, "\n\n")
		}
	}

	return errors
}

func getQualifiedObjectName(types *types.Package, value types.Object) string {
	pkg := value.Pkg()
	if pkg == nil || pkg == types {
		return value.Name()
	}

	return pkg.Name() + "." + value.Name()
}

func updateIdentNames(idents []*ast.Ident, name string) {
	for _, id := range idents {
		id.Name = name
	}
}

type GenericFunction struct {
	Function   *types.Func
	TypeParams []*types.TypeName
	Targets    []*GenericFunctionTarget
}

type GenericFunctionTarget struct {
	Name     string
	TypeArgs []*types.TypeName
}

func (g *GenericFunction) Generate(pkg *packages.Package, defs definitionLookup, src *ast.File, w io.Writer) []error {
	decl := defs.FindFunction(g.Function)
	if decl == nil {
		return []error{fmt.Errorf("could not locate function declaration for '%v'", g.Function)}
	}

	var name = []*ast.Ident{}
	var typeParams = make([][]*ast.Ident, len(g.TypeParams))
	for i := range typeParams {
		typeParams[i] = []*ast.Ident{}
	}

	astrewrite.Walk(decl, func(node ast.Node) (result ast.Node, cont bool) {
		result, cont = node, true

		id, ok := node.(*ast.Ident)
		if !ok {
			return
		}

		switch obj := pkg.TypesInfo.ObjectOf(id).(type) {
		case *types.Func:
			if obj == g.Function {
				name = append(name, id)
				return
			}

		case *types.TypeName:
			for i, typ := range g.TypeParams {
				if obj == typ {
					typeParams[i] = append(typeParams[i], id)
					return
				}
			}
		}

		return
	})

	var errors = []error{}
	if name == nil {
		errors = append(errors, fmt.Errorf("failed to locate identifier for function name"))
	}
	for i, typ := range typeParams {
		if typ == nil {
			errors = append(errors, fmt.Errorf("failed to locate identifier for type parameter %d", i))
		}
	}
	if len(errors) > 0 {
		return errors
	}

	for _, t := range g.Targets {
		updateIdentNames(name, t.Name)
		for i, typ := range typeParams {
			updateIdentNames(typ, getQualifiedObjectName(pkg.Types, t.TypeArgs[i]))
		}

		err := printer.Fprint(w, pkg.Fset, decl)
		if err != nil {
			errors = append(errors, err)
		} else {
			fmt.Fprintf(w, "\n\n")
		}
	}

	return errors
}
