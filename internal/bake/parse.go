package bake

import (
	"fmt"
	"go/ast"
	"go/token"
	"go/types"
	"strconv"
	"strings"

	"golang.org/x/tools/go/packages"
)

// BakePackageHistory is the history of package names used by Bake
var BakePackageHistory = [...]string{
	"gitlab.com/firelizzard/bake",
}

func resolveBakeImport(path string) (string, bool) {
	// TODO(light) This is depending on details of the current loader.
	const vendorPart = "vendor/"
	if i := strings.LastIndex(path, vendorPart); i != -1 && (i == 0 || path[i-1] == '/') {
		path = path[i+len(vendorPart):]
	}

	// check if path equals PKG or starts with PKG/ for each PKG in BakePackageHistory
	for _, pkg := range BakePackageHistory {
		// if PATH is shorter than PKG, they cannot match
		if len(path) < len(pkg) {
			continue
		}

		// verify that PKG is a prefix of path
		if path[:len(pkg)] != pkg {
			continue
		}

		// if PATH and PKG match in length, then they are equal
		if len(path) == len(pkg) {
			return "", true
		}

		// PATH is longer than PKG, check if prefixed with PKG/
		if path[len(pkg)] == '/' {
			return path[len(pkg)+1:], true
		}
	}

	return "", false
}

func isGenericGeneratorContext(typ string) bool {
	resolved, ok := resolveBakeImport(typ)
	if !ok {
		return false
	}

	return resolved == "generic.GeneratorContext"
}

func parse(pkg *packages.Package, defs definitionLookup) (map[*ast.File][]Generator, []error) {
	var allErrs = []error{}
	var allGenerators = map[*ast.File][]Generator{}

	for _, f := range pkg.Syntax {
		for _, decl := range f.Decls {
			defs.AddDecl(pkg.TypesInfo, decl)

			fn, ok := decl.(*ast.FuncDecl)
			if !ok {
				continue
			}

			// skip methods or external functions
			if fn.Recv != nil || fn.Body == nil {
				continue
			}

			// look for Generator signature
			if len(fn.Type.Params.List) != 1 || fn.Type.Results != nil {
				continue
			}
			sig := pkg.TypesInfo.ObjectOf(fn.Name).Type().(*types.Signature)
			if !isGenericGeneratorContext(sig.Params().At(0).Type().String()) {
				continue
			}

			for _, stmt := range fn.Body.List {
				switch stmt := stmt.(type) {
				case *ast.ExprStmt:
					call, ok := stmt.X.(*ast.CallExpr)
					if !ok {
						continue
					}

					calls, errs := unwindFluentCallStack(pkg, call)
					if len(errs) != 0 {
						allErrs = append(allErrs, errs...)
						continue
					}

					gen, errs := unpackGenerator(pkg, calls)
					if len(errs) != 0 {
						allErrs = append(allErrs, errs...)
					}
					if gen == nil {
						continue
					}

					generators, ok := allGenerators[f]
					if ok {
						allGenerators[f] = append(generators, gen)
					} else {
						allGenerators[f] = []Generator{gen}
					}
				}
			}
		}
	}

	return allGenerators, allErrs
}

type callInfo struct {
	Fun  *types.Func
	Args []ast.Expr
}

func unwindFluentCallStack(pkg *packages.Package, call *ast.CallExpr) ([]*callInfo, []error) {
	var calls = []*callInfo{}

	for {
		selector, ok := call.Fun.(*ast.SelectorExpr)
		if !ok {
			// not a fluent call
			return nil, nil
		}

		selection, ok := pkg.TypesInfo.Selections[selector]
		if !ok || selection.Kind() != types.MethodVal {
			return nil, nil
		}

		fun := selection.Obj().(*types.Func)
		calls = append(calls, &callInfo{Fun: fun, Args: call.Args})

		switch x := selector.X.(type) {
		case *ast.CallExpr:
			call = x
			continue

		case *ast.Ident:
			// ensure that the target is a GeneratorContext
			target := pkg.TypesInfo.ObjectOf(x)
			if target == nil || !isGenericGeneratorContext(target.Type().String()) {
				return nil, nil
			}

			// reverse the calls
			l := len(calls)
			for i := 0; i < l/2; i++ {
				calls[i], calls[l-i-1] = calls[l-i-1], calls[i]
			}

			return calls, nil

		default:
			// unexpected target type
			return nil, nil
		}
	}
}

func unpackGenerator(pkg *packages.Package, calls []*callInfo) (Generator, []error) {
	switch calls[0].Fun.Name() {
	case "Type":
		return unpackGenericTypeDefinition(pkg, calls)

	case "Method":
		return unpackGenericMethodDefinition(pkg, calls)

	case "Function":
		return unpackGenericFunctionDefinition(pkg, calls)

	default:
		return nil, []error{fmt.Errorf("failed to unpack generator: unknown generator '%s'", calls[0].Fun.Name())}
	}
}

func unpackGenericTypeDefinition(pkg *packages.Package, calls []*callInfo) (Generator, []error) {
	typeParamCount, targetCount := countParametersAndTargets(calls)

	errs := []error{}
	gen := &GenericType{
		TypeParams: make([]*types.TypeName, 0, typeParamCount),
		Targets:    make([]*GenericTypeTarget, 0, targetCount),
	}

	if len(calls[0].Args) != 1 {
		// this should not be possible
		errs = append(errs, fmt.Errorf("failed to unpack type generator: incorrect number of arguments passed to Type"))
	} else if typ, err := getNamedTypeOf(pkg, calls[0].Args[0]); err != nil {
		errs = append(errs, fmt.Errorf("failed to unpack type generator: %s", err.Error()))
	} else {
		gen.Type = typ
	}

	processGenericGeneratorCalls(pkg, calls[1:], &errs,
		func(typ *types.TypeName) {
			gen.TypeParams = append(gen.TypeParams, typ)
		},
		func(call *callInfo) {
			target := &GenericTypeTarget{
				TypeArgs: make([]*types.TypeName, 0, len(call.Args)-1),
			}
			gen.Targets = append(gen.Targets, target)

			target.Name = expectStringLiteral(call.Args[0], &errs)

			for i, arg := range call.Args[1:] {
				getTypeArg(pkg, i+1, arg, &errs, func(typ *types.TypeName) {
					target.TypeArgs = append(target.TypeArgs, typ)
				})
			}
		})

	return gen, errs
}

func unpackGenericMethodDefinition(pkg *packages.Package, calls []*callInfo) (Generator, []error) {
	typeParamCount, targetCount := countParametersAndTargets(calls)

	errs := []error{}
	gen := &GenericMethod{
		TypeParams: make([]*types.TypeName, 0, typeParamCount),
		Targets:    make([]*GenericMethodTarget, 0, targetCount),
	}

	if len(calls[0].Args) != 1 {
		// this should not be possible
		errs = append(errs, fmt.Errorf("failed to unpack method generator: incorrect number of arguments passed to Method"))
	} else if typ, meth, err := getMethod(pkg, calls[0].Args[0]); err != nil {
		errs = append(errs, fmt.Errorf("failed to unpack method generator: %s", err.Error()))
	} else {
		gen.Method = meth
		gen.ReceiverType = typ
	}

	processGenericGeneratorCalls(pkg, calls[1:], &errs,
		func(typ *types.TypeName) {
			gen.TypeParams = append(gen.TypeParams, typ)
		},
		func(call *callInfo) {
			target := &GenericMethodTarget{
				TypeArgs: make([]*types.TypeName, 0, len(call.Args)-1),
			}
			gen.Targets = append(gen.Targets, target)

			getTypeArg(pkg, 0, call.Args[0], &errs, func(typ *types.TypeName) {
				target.ReceiverType = typ
			})
			for i, arg := range call.Args[1:] {
				getTypeArg(pkg, i+1, arg, &errs, func(typ *types.TypeName) {
					target.TypeArgs = append(target.TypeArgs, typ)
				})
			}
		})

	return gen, errs
}

func getFunction(pkg *packages.Package, expr ast.Expr) (*types.Func, error) {
	switch expr := expr.(type) {
	case *ast.Ident:
		obj := pkg.TypesInfo.ObjectOf(expr)
		if obj == nil {
			return nil, fmt.Errorf("cannot resolve identifier '%v'", expr)
		}

		fun, ok := obj.(*types.Func)
		if !ok {
			return nil, fmt.Errorf("not a function: %v", obj)
		}

		return fun, nil

	case *ast.SelectorExpr:
		if id, ok := expr.X.(*ast.Ident); ok {
			if p, ok := pkg.TypesInfo.ObjectOf(id).(*types.PkgName); ok {
				return nil, fmt.Errorf("function is in another package: %v.%v", p.Imported().Path(), expr.Sel)
			}
		}

		sel, ok := pkg.TypesInfo.Selections[expr]
		if !ok {
			return nil, fmt.Errorf("cannot resolve selector '%v.%v'", expr.X, expr.Sel)
		}

		if sel.Kind() == types.FieldVal {
			return nil, fmt.Errorf("'%v.%v' is not a method", expr.X, expr.Sel)
		}

		return sel.Obj().(*types.Func), nil

	default:
		return nil, fmt.Errorf("cannot resolve function from %T", expr)
	}
}

func getMethod(pkg *packages.Package, expr ast.Expr) (*types.TypeName, *types.Func, error) {
	selector, ok := expr.(*ast.SelectorExpr)
	if !ok {
		return nil, nil, fmt.Errorf("cannot resolve method from %T", expr)
	}

	if id, ok := selector.X.(*ast.Ident); ok {
		if p, ok := pkg.TypesInfo.ObjectOf(id).(*types.PkgName); ok {
			return nil, nil, fmt.Errorf("function is in another package: %v.%v", p.Imported().Path(), selector.Sel)
		}
	}

	selection, ok := pkg.TypesInfo.Selections[selector]
	if !ok {
		return nil, nil, fmt.Errorf("cannot resolve selector %v", selector)
	}

	if selection.Kind() == types.FieldVal {
		return nil, nil, fmt.Errorf("expected method, got field")
	}

	named, ok := selection.Recv().(*types.Named)
	if !ok {
		return nil, nil, fmt.Errorf("'%v' is not a named type", selection.Recv())
	}

	return named.Obj(), selection.Obj().(*types.Func), nil
}

func unpackGenericFunctionDefinition(pkg *packages.Package, calls []*callInfo) (Generator, []error) {
	typeParamCount, targetCount := countParametersAndTargets(calls)

	errs := []error{}
	gen := &GenericFunction{
		TypeParams: make([]*types.TypeName, 0, typeParamCount),
		Targets:    make([]*GenericFunctionTarget, 0, targetCount),
	}

	if len(calls[0].Args) != 1 {
		// this should not be possible
		errs = append(errs, fmt.Errorf("failed to unpack function generator: incorrect number of arguments passed to Function"))
	} else if fun, err := getFunction(pkg, calls[0].Args[0]); err != nil {
		errs = append(errs, fmt.Errorf("failed to unpack function generator: %v", err.Error()))
	} else {
		gen.Function = fun
	}

	processGenericGeneratorCalls(pkg, calls[1:], &errs,
		func(typ *types.TypeName) {
			gen.TypeParams = append(gen.TypeParams, typ)
		},
		func(call *callInfo) {
			target := &GenericFunctionTarget{
				TypeArgs: make([]*types.TypeName, 0, len(call.Args)-1),
			}
			gen.Targets = append(gen.Targets, target)

			target.Name = expectStringLiteral(call.Args[0], &errs)

			for i, arg := range call.Args[1:] {
				getTypeArg(pkg, i+1, arg, &errs, func(typ *types.TypeName) {
					target.TypeArgs = append(target.TypeArgs, typ)
				})
			}
		})

	return gen, errs
}

func getNamedTypeOf(pkg *packages.Package, expr ast.Expr) (*types.TypeName, error) {
	typ := pkg.TypesInfo.TypeOf(expr)
	if typ == nil {
		return nil, fmt.Errorf("could not determine type of expression")
	}

	named, ok := typ.(*types.Named)
	if !ok {
		return nil, fmt.Errorf("'%v' is not a named type", typ.String())
	}

	return named.Obj(), nil
}

func getIdentObject(pkg *packages.Package, expr ast.Expr) types.Object {
	switch expr := expr.(type) {
	case *ast.Ident:
		return pkg.TypesInfo.ObjectOf(expr)

	case *ast.SelectorExpr:
		pkgName, ok := expr.X.(*ast.Ident)
		if !ok {
			return nil
		}

		if _, ok := pkg.TypesInfo.ObjectOf(pkgName).(*types.PkgName); !ok {
			return nil
		}

		return pkg.TypesInfo.ObjectOf(expr.Sel)

	default:
		return nil
	}
}

func getTypeArg(pkg *packages.Package, i int, arg ast.Expr, errs *[]error, use func(*types.TypeName)) bool {
	typ, err := getNamedTypeOf(pkg, arg)
	if err != nil {
		*errs = append(*errs, fmt.Errorf("failed to unpack generator parameter %d: %s", i+1, err.Error()))
		return false
	}

	use(typ)
	return true
}

// precompute array capacities to reduce reallocation
func countParametersAndTargets(calls []*callInfo) (typeParamCount int, targetCount int) {
	for _, call := range calls[1:] {
		switch call.Fun.Name() {
		case "Parameterize":
			typeParamCount += len(call.Args)

		case "Generate":
			targetCount++
		}
	}
	return
}

func processGenericGeneratorCalls(pkg *packages.Package, calls []*callInfo, errs *[]error, consumeTypeParameter func(*types.TypeName), consumeGenerateCall func(*callInfo)) {
	for _, call := range calls {
		switch call.Fun.Name() {
		case "Parameterize":
			for i, arg := range call.Args {
				getTypeArg(pkg, i, arg, errs, consumeTypeParameter)
			}

		case "Generate":
			if len(call.Args) < 1 {
				*errs = append(*errs, fmt.Errorf("failed to unpack generator target: not enough parameters"))
				return
			}

			consumeGenerateCall(call)

		default:
			*errs = append(*errs, fmt.Errorf("failed to unpack generator: unknown method generator method '%s'", calls[0].Fun.Name()))
		}
	}
}

func expectStringLiteral(expr ast.Expr, errs *[]error) string {
	if lit, ok := expr.(*ast.BasicLit); !ok || lit.Kind != token.STRING {
		*errs = append(*errs, fmt.Errorf("failed to unpack generator target: first parameter must be a string"))
	} else if name, err := strconv.Unquote(lit.Value); err != nil {
		*errs = append(*errs, fmt.Errorf("failed to unpack generator target: failed to unquote string: %s", err.Error()))
	} else {
		return name
	}
	return ""
}
